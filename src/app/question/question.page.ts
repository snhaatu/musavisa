import { Component, OnInit } from '@angular/core';
import { MusicQuestion } from '../../musicquestion';
import { Router } from '@angular/router';
import { compileNgModule } from '@angular/compiler';


@Component({
  selector: 'app-question',
  templateUrl: 'question.page.html',
  styleUrls: ['question.page.scss'],
})
export class QuestionPage implements OnInit {

  // Member variables
  questions: MusicQuestion[] = [];
  activeQuestion: MusicQuestion;
  isCorrect: boolean;
  feedback: string;
  questionCounter: number; 
  optionCounter: number;
  startTime: Date;
  endTime: Date;
  duration: number;
  correct: number
  nbrOfQuestions: number;
  isDisabled: boolean;
 


  constructor( public router: Router  ) {}

  ngOnInit() {
    fetch('../assets/data/datajson.json').then(res => res.json())
    .then(json => {
      this.questions = json;
       this.activeQuestion = this.questions[this.questionCounter];
    }); 

    this.setQuestion();
    
    this.questionCounter = 0;
    this.correct=0;
    this.startTime = new Date;
    this.isDisabled = false;
   
      
     
 }
 setQuestion() {
   this.questionCounter++
   this.isDisabled = false;
   if (this.questionCounter === this.questions.length ) {
        this.endTime = new Date;
        this.duration = this.endTime.getTime() - this.startTime.getTime();
        this.router.navigateByUrl('result/' + this.duration +
        '/' + this.questions.length +
        '/' + this.correct);
       
    } else {
        this.optionCounter = 0;
        this.feedback = '';
        this.isCorrect = false;
        this.activeQuestion = this.questions[this.questionCounter];
        
       
       
   }
 }

 checkOption(option : number, activeQuestion: MusicQuestion) {
   this.optionCounter++;
   if(this.optionCounter > activeQuestion.options.length) {
     this.setQuestion();
   }
   if (option === activeQuestion.correctOption) {
     this.isCorrect = true;
     this.correct++
     this.feedback = 'You knew the artist!';
     this.isDisabled = true;
   }else {
     this.isCorrect = false;
     this.feedback = 'Wrong, you should listen to more music.';
     this.isDisabled = true;

   }
 }



}  // end of Homepage
