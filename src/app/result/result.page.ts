import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {

  //Member variables
  duration: number;
  durationSeconds: number;
  correct: number;
  nbrOfQuestions: number;
  correctness: number
  level: string;
  source: string;

  constructor(public router: Router, public activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.duration = Number(this.activatedRoute.snapshot.paramMap.get('duration'));
    this.durationSeconds = Math.round((this.duration) / 1000);
     this.correct = Number(this.activatedRoute.snapshot.paramMap.get('correct'));
    this.nbrOfQuestions = Number(this.activatedRoute.snapshot.paramMap.get('nbrOfQuestions'));
    this.correctness = (this.correct / this.nbrOfQuestions) * 100;
    this.checkLevel()

    console.log(this.level)
  }

  

  goHome() {
    this.router.navigateByUrl('answers');
  }

  checkLevel() {
    switch (this.correct) {
      case 0:
          this.source = "../../assets/imgs/vanilja.png";
          this.level = "No no no, You are as fake as Vanilla Ice";
      break;
      case 1:
          this.source = "../../assets/imgs/lars.png";
          this.level = "Mehh, Your timing was right on one. Better than Lars Ulrich";
        break;
      case 2:
          this.source = "../../assets/imgs/axl.png";
          this.level = "Okay, 50/50 just like Axl Rose's career";
        break;
      case 3:
          this.source = "../../assets/imgs/lemmy.png";
          this.level= "Very good, Lemmy will have a Jack for you";
        break;
      case 4:
          this.source = "../../assets/imgs/diio.png";
          this.level =" Excellent! You make Dio proud, kudos."
    }
  }
  

}
