import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-answers',
  templateUrl: './answers.page.html',
  styleUrls: ['./answers.page.scss'],
})
export class AnswersPage implements OnInit {
  answers: [];

  constructor(public router: Router) { }

  ngOnInit() {
    fetch('../assets/data/datajson.json').then(res => res.json())
    .then(json => {
      this.answers = json;
       console.log(this.answers);
    }); 
  }

  
  goHome() {
    this.router.navigateByUrl('home');
  }

}
